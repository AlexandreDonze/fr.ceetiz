package fr.ceetiz.integrationtests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.ceetiz.model.AutoEntreprise;
import fr.ceetiz.model.Company;
import fr.ceetiz.model.Sas;
import fr.ceetiz.service.FinanceMinistryService;
import fr.ceetiz.serviceImpl.FinanceMinistryServiceImpl;


public class FinanceMinistryServiceImplTest {

	private static final String AUTO_ENTREPRISE_SIRET = "AUTO_ENTREPRISE_SIRET";
	private static final String AUTO_ENTREPRISE_DENOMINATION = "AUTO_ENTREPRISE_DENOMINATION";
	private static final float AUTO_ENTREPRISE_CA = 100_000f;
	private static final float AUTO_ENTREPRISE_TAXE = AUTO_ENTREPRISE_CA * 0.25f;
	
	private static final String CEETIZ_SIRET = "529 957 482";
	private static final String CEETIZ_DENOMINATION = "CEETIZ";
	private static final String CEETIZ_ADRESSE_SIEGE_SOCIAL = "13 RUE DES SABLONS 75116 PARIS";
	private static final float CEETIZ_ENTREPRISE_CA = 850_000_000f;
	private static final float CEETIZ_TAXE = CEETIZ_ENTREPRISE_CA * 0.33f;
	
	@Test
	public void testAutoEntreprise() {
		
		Company autoEntreprise = new AutoEntreprise(AUTO_ENTREPRISE_SIRET, AUTO_ENTREPRISE_DENOMINATION);

		FinanceMinistryService service = new FinanceMinistryServiceImpl();
		assertEquals(AUTO_ENTREPRISE_TAXE, service.impot(autoEntreprise, AUTO_ENTREPRISE_CA), .0f);
	}

	@Test
	public void testSas() {
		
		Company ceetiz = new Sas(CEETIZ_SIRET, CEETIZ_DENOMINATION, CEETIZ_ADRESSE_SIEGE_SOCIAL);

		FinanceMinistryService service = new FinanceMinistryServiceImpl();
		assertEquals(CEETIZ_TAXE, service.impot(ceetiz, CEETIZ_ENTREPRISE_CA), .0f);
	}

}
