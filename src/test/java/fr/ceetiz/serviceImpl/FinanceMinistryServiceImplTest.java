package fr.ceetiz.serviceImpl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import fr.ceetiz.model.Sas;
import fr.ceetiz.service.FinanceMinistryService;

// Pour le cas présent ce test est overkill mais je voulais le faire pour le principe des bonnes pratiques
public class FinanceMinistryServiceImplTest {
	
	private final float CA = 100_000f;
	private final float TAXE = .33f;
	private final float IMPOT = CA * TAXE;

	@Test
	public void test() {
		Sas sas = mock(Sas.class);
		when(sas.getTaxe()).thenReturn(TAXE);
		
		FinanceMinistryService service = new FinanceMinistryServiceImpl();
		assertEquals(IMPOT, service.impot(sas, CA), .0f);
	}

}
