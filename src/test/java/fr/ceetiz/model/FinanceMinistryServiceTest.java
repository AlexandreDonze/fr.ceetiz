package fr.ceetiz.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FinanceMinistryServiceTest {
	
	private static final String SIRET = "SIRET_NUMBER"; 
	private static final String DENOMINATION = "DENOMINATION";
	private static final String ADRESSE_SIEGE_SOCIAL = "ADRESSE_SIEGE_SOCIAL";
	private static final float TAXE = .33f;

	@Test
	public void test() {
		Sas sas = new Sas(SIRET, DENOMINATION, ADRESSE_SIEGE_SOCIAL);
		assertEquals(SIRET, sas.getNumeroSiret());
		assertEquals(DENOMINATION, sas.getDenomination());
		assertEquals(ADRESSE_SIEGE_SOCIAL, sas.getAdresseSiegeSocial());
		assertEquals(TAXE, sas.getTaxe(), .0f);
	}
}
