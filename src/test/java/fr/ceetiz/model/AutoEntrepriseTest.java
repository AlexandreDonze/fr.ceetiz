package fr.ceetiz.model;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class AutoEntrepriseTest {
	
	private static final String SIRET = "SIRET_NUMBER"; 
	private static final String DENOMINATION = "DENOMINATION";
	private static final float TAXE = .25f;

	@Test
	public void test() {
		AutoEntreprise autoEntreprise = new AutoEntreprise(SIRET, DENOMINATION);
		assertEquals(SIRET, autoEntreprise.getNumeroSiret());
		assertEquals(DENOMINATION, autoEntreprise.getDenomination());
		assertEquals(TAXE, autoEntreprise.getTaxe(), .0f);
	}

}
