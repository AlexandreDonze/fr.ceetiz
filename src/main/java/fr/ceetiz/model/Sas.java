package fr.ceetiz.model;

public class Sas extends Company {
	
	private static final float TAXE = .33f;
	private final String adresseSiegeSocial;
	
	public Sas(String numeroSiret, String denomination, String adresseSiegeSocial) {
		super(numeroSiret, denomination, TAXE);
		this.adresseSiegeSocial = adresseSiegeSocial;
	}

	public String getAdresseSiegeSocial() {
		return adresseSiegeSocial;
	}

}
