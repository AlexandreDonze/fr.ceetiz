package fr.ceetiz.model;

public abstract class Company {

	protected final String numeroSiret;
	protected final String denomination;
	protected final float taxe;
	
	protected Company(String numeroSiret, String denomination, float taxe) {
		this.numeroSiret = numeroSiret;
		this.denomination = denomination;
		this.taxe = taxe;
	}

	public String getNumeroSiret() {
		return numeroSiret;
	}

	public String getDenomination() {
		return denomination;
	}

	public float getTaxe() {
		return taxe;
	}

}
