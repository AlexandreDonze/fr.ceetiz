package fr.ceetiz.model;

public class AutoEntreprise extends Company {
	
	private static final float TAXE = .25f;

	public AutoEntreprise(String numeroSiret, String denomination) {
		super(numeroSiret, denomination, TAXE);
	}

}
