package fr.ceetiz.service;

import fr.ceetiz.model.Company;

public interface FinanceMinistryService {
	
	float impot(Company company, float ca);

}
