package fr.ceetiz.serviceImpl;

import fr.ceetiz.model.Company;
import fr.ceetiz.service.FinanceMinistryService;

public class FinanceMinistryServiceImpl implements FinanceMinistryService {

	@Override
	public float impot(Company company, float ca) {
		return ca * company.getTaxe();
	}

}
